require "spec_helper"
require "rails_helper"

describe DashboardController, type: :controller do
describe "index" do
    it "Landing page - dashboard" do
      get :index
      expect(response).to render_template("index")
    end
  end
end
