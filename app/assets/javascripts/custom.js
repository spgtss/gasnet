var ready;
ready = function() {
   $('#nominations').dataTable({
       "lengthChange": false,
       "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "Search"
        }
   })
   $('#notification').dataTable({
       "lengthChange": false,
       "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "Search"
        }
   })
   $('#schedule').dataTable({
       "lengthChange": false,
       "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "Search"
        }
   })
    
   //Custom class for search input
    $('.dataTables_filter input').addClass('search');
    
}
$(document).on('turbolinks:load', ready);